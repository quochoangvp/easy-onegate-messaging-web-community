"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");

let Post = require("./models/post");
let FB = require("fb");
module.exports = {
	settings: {
		name: "posts",
		version: 1,
		namespace: "posts",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Post,

		modelPropFilter: null,
		//"code title content author votes voters views createdAt editedAt detail",

		modelPopulates: {
			author: "people",
			voters: "people",
			fb_page: "page"
		}
	},

	actions: {
		fbPosts: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					//logger.info(ctx.req)
					let reqParams = ctx.req.body;

					if (!reqParams) {
						throw ctx.errorBadRequest(
							C.INVALID_FB_PARAM,
							ctx.t("app:IntegrationInvalidateParam")
						);
					}
					//FB.setAccessToken(ctx.user.profile.access_token);
					this.facebookService
						.graph(ctx)
						.then(response => {
							let fbconversations = response.posts;
							let pageId = response.id;
							let data = [];
							if (fbconversations) {
								data = fbconversations.data;
								if (data) {
									data.forEach(d => {
										//console.log("conversations ===================", fbconversations.id)
										logger.info(d);
										if (d.comments) {
											let comments = d.comments.data;
											comments.forEach(cm => {
												if (cm.comments) {
													let contacts =
														cm.comments.data;

													contacts.forEach(c => {
														console.log(
															"comment ===================",
															c
														);
														if (c.from) {
															// c.fb_pages = [
															// 	pageId
															// ];
															this.contactServce.updateOrInsert(
																c.from.id,
																c.from,
																pageId,
																function (
																	err,
																	result
																) {
																	if (err)
																		console.log(
																			err
																		);
																}
															);
														}
													});
												}
											});
										}
										if (d.likes) {
											let contacts = d.likes.data;
											contacts.forEach(c => {
												// c.fb_pages = [pageId];
												this.contactServce.updateOrInsert(
													c.id,
													c,
													pageId,
													function (err, result) {
														if (err)
															console.log(err);
													}
												);
											});
										}
										d.fb_page = pageId;
										this.updateOrInsert(d.id, d, function (
											err,
											result
										) {
											if (err) {
												console.log(err);
											}
										});
									});
									resolve(fbconversations);
								}
							} else {
								//ctx.assertModelIsExist(ctx.t("app:PostNotFound"));
								resolve({data: data});
							}
						})
						.catch(err => {
							reject(err);
						});
					// FB.api(...reqParams, response => {
					// 	if (!response || response.error) {
					// 		this.notifyModelChanges(
					// 			ctx,
					// 			"tokenExpired",
					// 			response.error
					// 		);
					// 		reject(response.error);
					// 	} else {
					// 		//resolve(response);
					// 		console.log("fbPosts===================", response);
					// 		//this.notifyModelChanges(ctx, "conversationloaded", response);

					// 	}
					// });
				});
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				// let filter = {};
				//
				// if (ctx.params.filter == "my") filter.author = ctx.user.id;
				// else if (ctx.params.author != null) {
				// 	filter.author = this.personService.decodeID(
				// 		ctx.params.author
				// 	);
				// }
				//
				// let query = Post.find(filter);
				//
				// return ctx
				// 	.queryPageSort(query)
				// 	.exec()
				// 	.then(docs => {
				// 		console.log(docs);
				// 		return this.toJSON(docs);
				// 	})
				// 	.then(json => {
				// 		console.log(json);
				// 		return this.populateModels(json);
				// 	});
				let filter = {};

				if (ctx.params.filter && ctx.params.filter.viewMode == "my")
					filter.author = ctx.user.id;
				else if (ctx.params.author != null) {
					filter.author = this.context
						.services("people")
						.decodeID(ctx.params.author);
				}
				if (ctx.params.condition)
					filter = _.assign(filter, ctx.params.condition);
				let query = this.collection.find(filter);

				return ctx
					.queryPageSort(query)

					.exec()
					.then(docs => {
						return this.toJSON(docs);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}
		},

		// return a model by ID
		get: {
			cache: false, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

				return Post.findByIdAndUpdate(ctx.modelID, {
					$inc: {views: 1}
				})
					.exec()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let post = new Post(ctx.params);
				post.author = ctx.user.id;
				return post
					.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					this.facebookService
						.graphFbModel(ctx)
						.then(result => {
							resolve(result)
							// return this.collection
							// 	.findById(ctx.modelID)
							// 	.exec()
							// 	.then(doc => {
							// 		if (ctx.params.title != null)
							// 			doc.title = ctx.params.title;
							//
							// 		if (ctx.params.content != null)
							// 			doc.content = ctx.params.content;
							//
							// 		doc.editedAt = Date.now();
							// 		return doc.save();
							// 	})
							// 	.then(doc => {
							// 		return this.toJSON(doc);
							// 	})
							// 	.then(json => {
							// 		return this.populateModels(json);
							// 	})
							// 	.then(json => {
							// 		this.notifyModelChanges(ctx, "updated", json);
							// 		return json;
							// 	});
						})
						.catch(err => {
							reject(err);
						});
				});
			}
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

				return Post.remove({_id: ctx.modelID})
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) !== -1)
						throw ctx.errorBadRequest(
							C.ERR_ALREADY_VOTED,
							ctx.t("app:YouHaveAlreadyVotedThisPost")
						);
					return doc;
				})
				.then(doc => {
					// Add user to voters
					return Post.findByIdAndUpdate(
						doc.id,
						{
							$addToSet: {voters: ctx.user.id},
							$inc: {votes: 1}
						},
						{new: true}
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "voted", json);
					return json;
				});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) == -1)
						throw ctx.errorBadRequest(
							C.ERR_NOT_VOTED_YET,
							ctx.t("app:YouHaveNotVotedThisPostYet")
						);
					return doc;
				})
				.then(doc => {
					// Remove user from voters
					return Post.findByIdAndUpdate(
						doc.id,
						{$pull: {voters: ctx.user.id}, $inc: {votes: -1}},
						{new: true}
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "unvoted", json);
					return json;
				});
		}
	},

	methods: {
		findByOneId(id) {
			return new Promise((resolve, reject) => {
				this.collection
					.find(
						{fb_id: _.cloneDeep(id)}
					)
					.exec()
					.then(result => {
						resolve(this.toJSON(result));
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		updateOrInsert(id, model, cb) {
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false
				};
				this.collection
					.findOneAndUpdate(
						{fb_id: _.cloneDeep(id)},
						{$set: _.cloneDeep(model)},
						options
					)
					.exec()
					.then(result => {
						if (cb) cb(undefined, result);
						resolve(this.toJSON(result));
					})
					.catch(err => {
						if (cb) cb(err, undefined);
						reject(err);
					});
			});
		},
		saveOrInsert(id, model, cb) {
			let post = _.cloneDeep(model);
			post.fb_id = id;
			delete model.id;
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false
				};
				this.collection
					.findOneAndUpdate(
						{fb_id: _.cloneDeep(id)},
						{$set: _.cloneDeep(post)},
						options
					)
					.exec()
					.then(result => {
						if (cb) cb(undefined, result);
						resolve(this.toJSON(result));
					})
					.catch(err => {
						if (cb) cb(err, undefined);
						reject(err);
					});
			});
		},
		updateOrInsertCount(searchObj, updateParams, options, cb) {
			return this.collection.find(
				searchObj,
				(err, existingCommentOrPhone) => {
					if (existingCommentOrPhone.length == 0) {
						return [];
					} else if (existingCommentOrPhone.length > 0) {
						let promises = [];
						let results = [];
						options = options
							? options
							: {
								// upsert: true,
								new: true,
								setDefaultsOnInsert: true
							};

						existingCommentOrPhone.forEach(item => {
							promises.push(
								this.collection
									.findByIdAndUpdate(
										item.id,
										updateParams,
										options
									)
									.exec()
									.then(json => {
										// if(cb){
										// 	let noctx = CONTEXT.CreateToServiceInit(this.context.services("notifications"));
										// 	let objecToSend = this.toJSON(json);
										// 	objecToSend.keepPosition = true;
										// 	var bro = {method: "conversationChanged", value: objecToSend};
										// 	noctx.broadcast("notification", bro);
										// }
										results.push(json);
									})
									.catch(err => {
										results.push(err);
									})
							);
						});

						if (promises.length > 0) {
							Promise.all(promises).then(response => {
								return results;
							});
						}
					} else {
						let data = {
							success: false,
							msg: "Unknown",
							data: existingCommentOrPhone
						};
						return this.notifyModelChanges(ctx, "Error", data);
					}
				}
			);
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title")
					.trim()
					.notEmpty(ctx.t("app:PostTitleCannotBeEmpty"))
					.end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content")
					.trim()
					.notEmpty(ctx.t("app:PostContentCannotBeEmpty"))
					.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		}
	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else reject();
		});
	},

	init(ctx) {
		// Fired when start the service
		this.personService = ctx.services("people");
		this.contactServce = ctx.services("contacts");
		this.facebookService = ctx.services("facebook");
		// Add custom error types
		C.append(["ALREADY_VOTED", "NOT_VOTED_YET"], "ERR");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {
		query: `
            posts(limit: Int, offset: Int, sort: String): [Post]
            post(code: String): Post
        `,

		types: `
            type Post {
                code: String!
                title: String
                content: String
                author: Person!
                views: Int
                votes: Int,
                voters(limit: Int, offset: Int, sort: String): [Person]
                createdAt: Timestamp
                createdAt: Timestamp
            }
        `,

		mutation: `
            postCreate(title: String!, content: String!): Post
            postUpdate(code: String!, title: String, content: String): Post
            postRemove(code: String!): Post

            postVote(code: String!): Post
            postUnvote(code: String!): Post
        `,

		resolvers: {
			Query: {
				posts: "find",
				post: "get"
			},

			Mutation: {
				postCreate: "create",
				postUpdate: "update",
				postRemove: "remove",
				postVote: "vote",
				postUnvote: "unvote"
			}
		}
	}
};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
      voters {
        code
        fullName
        username
        avatar
      }
}

*/
