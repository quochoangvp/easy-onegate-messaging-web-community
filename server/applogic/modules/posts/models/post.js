"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("posts");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let PostSchema = new Schema({
	// _id: {
	// 	type: String,
	// 	unique: true,
	// 	trim: true
	// },
	fb_id: {
		type: String,
		unique: true,
		trim: true
	},
	fb_page :  
		{type: String, ref:"Page", field:"fb_id"}
	,
	title: {
		type: String,
		trim: true
	},
	detail: {
		type: String,
		trim: true
	},
	content: {
		type: String,
		trim: true
	},
	author: {
		type: String,
		//equired: "Please fill in an author ID",
		ref: "User"
	},
	views: {
		type: Number,
		default: 0
	},
	voters: [{
		type: String,
		ref: "User"
	}],
	votes: {
		type: Number,
		default: 0
	},
	editedAt: {
		type: Date
	},
	metadata: {}

}, schemaOptions);

PostSchema.virtual("code").get(function() {
	return this.encodeID();
});

// PostSchema.plugin(autoIncrement.plugin, {
// 	model: "Post",
// 	startAt: 1
// });

PostSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

PostSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

/*
PostSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Post = mongoose.model("Post", PostSchema);

module.exports = Post;
