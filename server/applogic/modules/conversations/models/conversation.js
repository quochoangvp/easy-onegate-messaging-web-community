"use strict";

// let ROOT 			= "../../../../";
let config = require("../../../../config");
let logger = require("../../../../core/logger");

let _ = require("lodash");

let db = require("../../../../core/mongo");
let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;
let hashids = require("../../../../libs/hashids")("conversations");
let autoIncrement = require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let ConversationSchema = new Schema(
	{
		fb_id: {
			type: String,
			trim: true,
			unique: true
		},
		fb_postId: {
			type: String,
			trim: true
		},
		fb_page: {
			type: String
		},
		comment_id: {
			type: String,
			unique: true,
			sparse: true
		},
		hasPhone : {
			type : Boolean
		},
		// comments: [
		// 	{id:{
		// 		type: ObjectId,
		// 		ref: "Comment",
		// 	}}
		// ],
		fb_type: {
			type: String
		},
		fb_fromId: {
			type: String,
			trim: true
		},
		from: {

		},
		last_interacted_by: {
			type: String,
			trim: true
		},
		last_seen_time: {
			type: Date
		},
		assignee_ids: {
			type: Array
		},
		ads_ids: {
			type: Array
		},
		customerIds: [{ type: String }],
		seen: {
			type: Boolean
		},
		recent_phone_numbers: [],
		recent_seen_userIds:
		{
			type: Array
		},
		tags: {
			type: Array,
		},
		thread_id: {
			type: String
		},
		thread_key: { type: String },
		link: {
			type: String
		},
		updated_time: {
			type: Date
		},
		created_time: {
			type: Date
		},
		read_watermark: {
			type: Date
		},
		snippet: {
			type: String
		},
		message_count: {
			type: Number,
			default: 0
		},
		comment_count: {
			type: Number,
			default: 0
		},
		participants: {
			type: Array
		},
		senders: {
			type: Array
		},
		last_sent_by: {

		},
		can_reply: {
			type: Boolean
		},
		is_subscribed: {
			type: Boolean
		},
		unread_count: {
			type: Number,
			default: 0
		},
		phone_count: {
			type: Number,
			default: 0
		},
		views: {
			type: Number,
			default: 0
		},
		voters: [{
			type: String,
			ref: "User"
		}],
		votes: {
			type: Number,
			default: 0
		},
		isUnread: {
			type: Number,
		},
		editedAt: {
			type: Date
		},
		metadata: {}
	},
	schemaOptions
);

ConversationSchema.post("findOneAndUpdate", function (doc) {
	if(doc.unread_count>0){
		doc.isUnread = 1;
	}else{
		doc.isUnread = 0;
	}
	doc.save();
});

ConversationSchema.virtual("code").get(function() {
	return this.encodeID();
});
// ConversationSchema.virtual("conversation_id").get(function() {
// 	if(this.fb_type == "COMMENT") return this.fb_postId + "_" + this.fb_fromId;
// 	else return this.thread_id;
// });

ConversationSchema.virtual("page", {
	ref: "Page", // The model to use
	localField: "fb_page", // Find people where `localField`
	foreignField: "fb_id" // is equal to `foreignField`
});
ConversationSchema.virtual("post", {
	ref: "Post", // The model to use
	localField: "fb_postId", // Find people where `localField`
	foreignField: "fb_id", // is equal to `foreignField`
	justOne: true
});
ConversationSchema.virtual("messages", {
	ref: "Message", // The model to use
	localField: "fb_id", // Find people where `localField`
	foreignField: "conversation_id",
	justOne: false // is equal to `foreignField`
});
ConversationSchema.virtual("comments", {
	ref: "Comment", // The model to use
	localField: "fb_id", // Find people where `localField`
	foreignField: "conversation_id",
	justOne: false // is equal to `foreignField`
});
ConversationSchema.virtual("recent_seen_users", {
	ref: "User", // The model to use
	localField: "recent_seen_userIds", // Find people where `localField`
	foreignField: "_id",
	justOne: false // is equal to `foreignField`
});
ConversationSchema.virtual("assignees", {
	ref: "User", // The model to use
	localField: "assignee_ids", // Find people where `localField`
	foreignField: "_id",
	justOne: false // is equal to `foreignField`
});

ConversationSchema.virtual("customers", {
	ref: "Contact", // The model to use
	localField: "fb_fromId", // Find people where `localField`
	foreignField: "fb_id",
	justOne: true // is equal to `foreignField`
});

ConversationSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

ConversationSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

/*
PostSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Conversation = mongoose.model("Conversation", ConversationSchema);

module.exports = Conversation;
