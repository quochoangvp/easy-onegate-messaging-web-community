"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("comments");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let MessageSchema = new Schema({
	created_by: {

	},
	message: {
		type: String
	},
	text: {
		type: String
	},
	banned_count: {
		type: Number,
		default: 0
	},
	birthday: {
		type : Date
	} ,
	can_inbox : {
		type: Boolean
	},
	comment_count: {
		type: Number,
		default: 0
	} ,
	fb_id: {
		type: String,
		trim: true,
		unique: true,
		sparse: true
	},
	conversation_id: {
		type: String
	},
	permalink_url: {
		type: String
	},

	inserted_at: {
		type: Date
	},
	is_hidden: {
	 type: Boolean
	},
	is_parent : {
		type: Boolean
	},
	is_parent_hidden : {
		type: Boolean
	},
	is_removed : {
		type: Boolean
	},
	verb : {
		type: String
	},
	like_count : {
		type: Number
	},
	author: {
		type: String,
		//required: "Please fill in an author ID",
		ref: "User"
	},
	views: {
		type: Number,
		default: 0
	},
	voters: [{
		type: String,
		ref: "User"
	}],
	votes: {
		type: Number,
		default: 0
	},
	editedAt: {
		type: Date
	},
	metadata: {}

}, schemaOptions);

MessageSchema.virtual("code").get(function() {
	return this.encodeID();
});

// MessageSchema.plugin(autoIncrement.plugin, {
// 	model: "Message",
// 	startAt: 1
// });

MessageSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

MessageSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};
MessageSchema.virtual("replies", {
	ref: "Comment", // The model to use
	localField: "fb_id", // Find people where `localField`
	foreignField: "parentId",
	justOne: false // is equal to `foreignField`
});
/*
MessageSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

//let Message = mongoose.model("Comment", MessageSchema);

module.exports = function(collectionName) {
	let	Message = mongoose.model(collectionName, MessageSchema);
	return Message;
};
