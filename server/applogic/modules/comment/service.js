"use strict";

let logger 		= require("../../../core/logger");
let C 	 		= require("../../../core/constants");
let _			= require("lodash");
let Comment 	= require("./models/comment")("Comment");

module.exports = {
	settings: {
		name: "comments",
		version: 1,
		namespace: "comments",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Comment,

		modelPropFilter: null, //"code title content author votes voters views createdAt editedAt",

		modelPopulates: {
			"author": "people",
			"voters": "people"
		}
	},

	actions: {
		sendAndSave:{
			handler(ctx) {
				return new Promise((resolve, reject) => {
					if(ctx.req.body && ctx.req.body.fbModel && ctx.req.body.message){
						this.facebookService.graphFbModel(ctx).then(msgout=>{
							let msg = _.cloneDeep(ctx.req.body.message);
							msg.id = msgout.id;
							msg.author = ctx.user.id;
							this.createLocalMessage(ctx, msg.from.id, msg);
							resolve(msgout);
						}).catch(err=>{
							reject(err);
						});
					} else {
						reject(ctx.t("app:NoContentToSend"));
					}
				});
			}
		},
		facebook: {
			handler(ctx) {
				return new Promise((resolve, reject) => {

					this.facebookService.graph(ctx).then(result=>{


						if(result && result.data){
							_.forEach(result.data, message=>{
								this.updateOrInsert(ctx, message.id, message);
							});
						}
						resolve(result);
					}).catch(err=>{
						reject(err);
					});
				});
				//return this.facebookService.graph(ctx);
			}
		},
		facebookExt: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					//console.log("mctx ======================",ctx)
					this.facebookService.graphFbModel(ctx).then(result=>{

						if(result && result.data){
							_.forEach(result.data, message=>{
								console.log("message fb ======================",message, ctx.req.body);
								this.updateOrInsert(ctx, message.id, message);
							});
						}
						resolve(result);
					}).catch(err=>{
						reject(err);
					});
				});
				//return this.facebookService.graph(ctx);
			}
		},
		sendMediaMessage: {
			cache: false,
			permission: C.PERM_LOGGEDIN,
			handler (ctx) {
				return new Promise((resolve, reject)=>{
					let attachment = ctx.req.body.fbModel.files[0];
					const messenger = new Messenger({
						pageAccessToken: ctx.app.locals.pageTokenCache.get(ctx.user.socialLinks.facebook +"_" + ctx.params.pageId)
					});

					// let buf = Buffer.from(attachment., "base64");

					messenger.send(new Image({
						url: "http://lorempixel.com/400/400/sports/1/",
					}), ctx.params.recipient).then(res => {
						resolve(res);
					}).catch(err => {
						resolve(err);
					});


				});
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				return this.findCommentsBy(ctx);
			}
		},

		// return a model by ID
		get: {
			cache: true, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

				return Post.findByIdAndUpdate(ctx.modelID, { $inc: { views: 1 } }).exec().then( (doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let message = new Message({
					title: ctx.params.title,
					content: ctx.params.content,
					author: ctx.user.id
				});

				return message.save()
				.then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "created", json);
					return json;
				});
			}
		},

		update: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));
				this.validateParams(ctx);

				return this.collection.findById(ctx.modelID).exec()
				.then((doc) => {
					if (ctx.params.title != null)
						doc.title = ctx.params.title;

					if (ctx.params.content != null)
						doc.content = ctx.params.content;

					doc.editedAt = Date.now();
					return doc.save();
				})
				.then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "updated", json);
					return json;
				});
			}
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

				return Message.remove({ _id: ctx.modelID })
				.then(() => {
					return ctx.model;
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "removed", json);
					return json;
				});
			}
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

			return this.collection.findById(ctx.modelID).exec()
			.then((doc) => {
				// Check user is on voters
				if (doc.voters.indexOf(ctx.user.id) !== -1)
					throw ctx.errorBadRequest(C.ERR_ALREADY_VOTED, ctx.t("app:YouHaveAlreadyVotedThisMessage"));
				return doc;
			})
			.then((doc) => {
				// Add user to voters
				return Post.findByIdAndUpdate(doc.id, { $addToSet: { voters: ctx.user.id } , $inc: { votes: 1 }}, { "new": true });
			})
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "voted", json);
				return json;
			});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

			return this.collection.findById(ctx.modelID).exec()
			.then((doc) => {
				// Check user is on voters
				if (doc.voters.indexOf(ctx.user.id) == -1)
					throw ctx.errorBadRequest(C.ERR_NOT_VOTED_YET, ctx.t("app:YouHaveNotVotedThisMessageYet"));
				return doc;
			})
			.then((doc) => {
				// Remove user from voters
				return Post.findByIdAndUpdate(doc.id, { $pull: { voters: ctx.user.id } , $inc: { votes: -1 }}, { "new": true });
			})
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "unvoted", json);
				return json;
			});

		},

	},

	methods: {
		findCommentsBy(ctx){
			return new Promise((resolve, reject)=>{
				let filter = ctx.params.filter;
				let mLimit = ctx.params.limit ? ctx.params.limit : 100;
				let offset = ctx.params.offset ? ctx.params.offset : 0;
				let order = ctx.params.order ? ctx.params.order : "-created_time";

				let options = {limit: mLimit, sort: order, skip: offset};

				if (ctx.params.filter == "my") filter.author = ctx.user.id;
				else if (ctx.params.author != null) {
					filter.author = this.personService.decodeID(
						ctx.params.author
					);
				}
				this.collection.find(filter, null, options).exec((err, docs) => {
					if(!err){
						resolve(docs);
					}else{
						reject(err);
					}
				});
			});
		},
		find(key) {
			return new Promise((resolve, reject) => {
				this.collection
					.find({ fb_id: key })
					.then(data => {
						resolve(data);
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		update(filter,options) {
			return new Promise((resolve, reject) => {
				this.collection.findOne(filter, options).exec()
					.then(doc => {
						resolve(doc);
					}).catch(err=>{
						reject(err);
					});
			});
		},
		createLocalMessage(ctx, senderId, message){
			 	if(ctx.user) {
					 message.author = ctx.user.id;
				 }
			let options = {
				upsert: true,
				new: true,
				setDefaultsOnInsert: true
			};
			let object = _.cloneDeep(message);
			if(object.id) {
				delete object.id;
				object.fb_id = message.id;
			}
			return this.collection
				.findOneAndUpdate(
					{ fb_id: message.id },
					{ $set: object },
					options
				)
				.exec().then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "messageSaved", json);
					return json;
				}).catch(err=>{
					logger.error(err);
					return Promise.reject(err);
				});

		},
		updateOrInsert(id, model,cb) {
			return new Promise((resolve, reject)=>{
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false
				};
				let object = _.cloneDeep(model);
				object.fb_id = id;
				delete object.id;
				this.collection
					.findOneAndUpdate(
						{ fb_id: id },
						{ $set: object },
						options
					)
					.exec()
					.then(result => {
						if(cb)
							cb(undefined, result);
						resolve(this.toJSON(result));
					})
					.catch(err => {
						console.error("save comment error ===================", err);
						if(cb)
							cb(err, undefined);
						reject(err);
					});
			});

		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title").trim().notEmpty(ctx.t("app:MessageTitleCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content").trim().notEmpty(ctx.t("app:MessageContentCannotBeEmpty")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		},
		searchBy(condition, fields){
			return this.collection.find(condition, fields).exec();
		}

	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else
				reject();
		});
	},

	init(ctx) {
		// Fired when start the service
		this.facebookService = ctx.services("facebook");
		this.personService = ctx.services("people");
		this.contactService = ctx.services("contacts");

		// Add custom error types
		C.append([
			"ALREADY_VOTED",
			"NOT_VOTED_YET"
		], "ERR");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		Messages(limit: Int, offset: Int, sort: String): [Comment]
		Message(code: String): Comment
		`,

		types: `
			type Comment {
				code: String!
				title: String
				content: String
				author: Person!
				views: Int
				votes: Int,
				voters(limit: Int, offset: Int, sort: String): [Person]
				createdAt: Timestamp
				createdAt: Timestamp
			}
		`,

		mutation: `
		MessageCreate(title: String!, content: String!): Comment
		MessageUpdate(code: String!, title: String, content: String): Comment
		MessageRemove(code: String!): Comment

		MessageVote(code: String!): Comment
		MessageUnvote(code: String!): Comment
		`,

		resolvers: {
			Query: {
				Messages: "find",
				Message: "get"
			},

			Mutation: {
				MessageCreate: "create",
				MessageUpdate: "update",
				MessageRemove: "remove",
				MessageVote: "vote",
				MessageUnvote: "unvote"
			}
		}
	}

};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
