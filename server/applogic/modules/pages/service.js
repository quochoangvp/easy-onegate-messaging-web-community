"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");

let Page = require("./models/page");
let User = require("../../../models/user");

let Contact = require("../contacts/models/contact");
let FB = require("fb");

module.exports = {
	settings: {
		name: "pages",
		version: 1,
		namespace: "pages",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Page,

		modelPropFilter: null,
			//"code title content author votes voters views createdAt editedAt",

		modelPopulates: {
			author: "people",
			voters: "people"
		}
	},

	actions: {
		fb: {
			handler(ctx) {
				return this.facebookService.graph(ctx);
				// return new Promise((resolve, reject) => {
				// 	let reqParams = ctx.req.body;
				// 	FB.setAccessToken(ctx.user.profile.access_token);
				// 	FB.api(...reqParams, response => {
				// 		if (!response || response.error) {
				// 			this.notifyModelChanges(
				// 				ctx,
				// 				"tokenExpired",
				// 				response.error
				// 			);
				// 			reject(response.error);
				// 		} else {
				// 			resolve(response);
				// 			// this.notifyModelChanges(ctx, "Pageloaded", response);
				// 			// let fbPages = response;
				// 			// let ids = _.keyBy(fbPages, "id");
				// 			// let filter = { id : {$in : ids}}
				// 			// let query = Page.find(filter);
				// 			// return ctx.queryPageSort(query).exec().then( (docs) => {

				// 			// 	return this.toJSON(docs);
				// 			// })
				// 			// .then((json) => {
				// 			// 	return this.populateModels(json);
				// 			// });
				// 			//resolve(response);
				// 		}
				// 	});
				// });
			}
		},
		fbPages: {
			cache: false,
			//permission: C.PERM_LOGGEDIN,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					this.facebookService.graph(ctx).then(response=>{
						let fbPages = response.accounts;
						let fbUser = response.id;
						if (fbPages) {
							let data = fbPages.data;
							if (data) {
								//NOTE update pages to offline pages
								data.forEach(d => {
									this.updateOrInsert(
										d.id,
										d,
										fbUser
									).then(page=>{
										this.notifyModelChanges(ctx, "created", page.name);
										this.assingPageToUser(d.id, ctx.user.id);
										if(d.tasks && d.tasks.indexOf("MANAGE") > -1) {
											this.promotePageToUser(d.id, ctx.user.id);
										}
										//d = page;
									}).catch(err=>{
										logger.error(err);
									});
								});
								//TODO using promise all to cach new updated page and return pages, on UI sould populate fb_id instead of id
								resolve(fbPages);
							}
						} else {
							reject(response);
						}
					}).catch(err=>{
						reject(err);
					});
				});
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				let filter = {isActive: true};
				console.log(ctx.user);
				//filter["fb_id"] = {$in: ctx.user.fb_pageIds};	
				filter = _.assign(filter, ctx.params.condition);
				let query = this.collection.find(filter);
				return ctx
					.queryPageSort(query)
					.exec()
					.then(docs => {						
						return this.toJSON(docs);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}
		},

		// return a model by ID
		get: {
			cache: true, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PageNotFound"));

				return Page.findByIdAndUpdate(ctx.modelID, {
					$inc: { views: 1 }
				})
					.exec()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let Page = new Page({
					title: ctx.params.title,
					content: ctx.params.content,
					author: ctx.user.id
				});

				return Page.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PageNotFound"));
				this.validateParams(ctx);

				return this.collection
					.findById(ctx.modelID)
					.exec()
					.then(doc => {
						if (ctx.params.title != null)
							doc.title = ctx.params.title;

						if (ctx.params.content != null)
							doc.content = ctx.params.content;

						doc.editedAt = Date.now();
						return doc.save();
					})
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					});
			}
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PageNotFound"));

				return Page.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:PageNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) !== -1)
						throw ctx.errorBadRequest(
							C.ERR_ALREADY_VOTED,
							ctx.t("app:YouHaveAlreadyVotedThisPost")
						);
					return doc;
				})
				.then(doc => {
					// Add user to voters
					return Page.findByIdAndUpdate(
						doc.id,
						{
							$addToSet: { voters: ctx.user.id },
							$inc: { votes: 1 }
						},
						{ new: true }
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "voted", json);
					return json;
				});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:PageNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) == -1)
						throw ctx.errorBadRequest(
							C.ERR_NOT_VOTED_YET,
							ctx.t("app:YouHaveNotVotedThisPageYet")
						);
					return doc;
				})
				.then(doc => {
					// Remove user from voters
					return Page.findByIdAndUpdate(
						doc.id,
						{ $pull: { voters: ctx.user.id }, $inc: { votes: -1 } },
						{ new: true }
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "unvoted", json);
					return json;
				});
		}
	},

	methods: {
		getPageByFbId(id, populate){
			return this.collection.find({fb_id:id}).populate(populate).exec().then(doc=>{
				return this.toJSON(doc);
			});
		},
		callToFb(reqParams, access_token) {
			return new Promise((resolve, reject) => {
				//let reqParams = ctx.req.body;
				if (access_token) FB.setAccessToken(access_token);
				if (reqParams)
					FB.api(...reqParams, response => {
						if (!response || response.error) {
							//this.notifyModelChanges(ctx, "tokenExpired", response.error);
							reject(response.error);
						} else {
							resolve(response);
						}
					});
				else {
					reject("reqParams is required!");
				}
			});
		},
		updateOrInsert(id, model, fbIid, cb) {
			return new Promise((resolve, reject)=>{
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false
				};
				let page = _.cloneDeep(model);
				page.fb_id = id;
				delete page.id;
				this.collection
				.findOneAndUpdate(
					{ fb_id: id },
					{
						$set: page
					},
					options
				)
				.exec()
				.then(doc => {
					if(cb)cb(undefined, doc);
					resolve(this.toJSON(doc));
				}).then(json=>{
					return this.populateModels(json);
				})
				.catch(err => {
					if(cb)cb(err, undefined);
					reject(err);
				});
			});
		},
		assingPageToUser(pageId, userId){
			return new Promise((resolve, reject)=>{
				User.findOneAndUpdate({_id: userId}, { $addToSet: {"profile.accessPages": pageId}},{upsert: true, new: true}).exec().then(doc=>{
					resolve(this.toJSON());
				}).catch(err=>{
					reject(err);
				});
			});
		},
		promotePageToUser(pageId, userId){
			return new Promise((resolve, reject)=>{
				User.findOneAndUpdate({_id: userId}, { $addToSet: {"profile.fbPages": pageId}},{upsert: true, new: true}).exec().then(doc=>{
					resolve(this.toJSON());
				}).catch(err=>{
					reject(err);
				});
			});
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title")
					.trim()
					.notEmpty(ctx.t("app:PageTitleCannotBeEmpty"))
					.end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content")
					.trim()
					.notEmpty(ctx.t("app:PageContentCannotBeEmpty"))
					.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		}
	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:PageNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else reject();
		});
	},

	init(ctx) {
		this.facebookService = ctx.services("facebook");
		this.conversationService = ctx.services("conversations");
		// Fired when start the service
		this.personService = ctx.services("people");

		// Add custom error types
		C.append(["ALREADY_VOTED", "NOT_VOTED_YET"], "ERR");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {
		query: `
		pages(limit: Int, offset: Int, sort: String): [Page]
		page(code: String): Page
		page(fb_id: String!): Page
		`,

		types: `
			type Page {
				code: String!
				name: String,
				fb_id: String,
				access_token: String,
				title: String
				content: String
				author: Person				
				views: Int
				votes: Int,
				voters(limit: Int, offset: Int, sort: String): [Person]
				createdAt: Timestamp
				createdAt: Timestamp
			}
		`,

		mutation: `
		pageCreate(title: String!, content: String!): Page
		pageUpdate(code: String!, title: String, content: String): Page
		pageRemove(code: String!): Page

		pageVote(code: String!): Page
		pageUnvote(code: String!): Page
		`,

		resolvers: {
			Query: {
				pages: "find",
				page: "get"
			},

			Mutation: {
				pageCreate: "create",
				pageUpdate: "update",
				pageRemove: "remove",
				pageVote: "vote",
				pageUnvote: "unvote"
			}
		}
	}
};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
