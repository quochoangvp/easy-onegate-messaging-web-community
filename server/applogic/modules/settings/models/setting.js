"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("settings");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
	//discriminatorKey: "service-type"
};

let SettingSchema = new Schema({
	// _id: {
	// 	type: String,
	// 	trim: true,
	// 	unique: true
	// },
	author: {
		type: String,
		// required: "Please fill in an author ID",
		ref: "User"
	},
	address: {
		type: String,
		trim: true
	},
	fb_link: {
		type: String,
		trim: true
	},
	fb_id: {
		type: String,
		trim: true
	},
	fb_pages: {
		type: Array
	},
	templates: {
		attachments: [
		 
		]
	},
	type: {
		type: String,
		trim: true
	},
	name: {
		type: String,
		trim: true
	},
	description: {
		type: String,
		trim: true,
		"default": ""
	},
	status: {
		type: Number,
		"default": 1
	},
	lastCommunication: {
		type: Date,
		"default": Date.now
	},
	metadata: {}

}, schemaOptions);

SettingSchema.virtual("code").get(function() {
	return this.encodeID();
});

// SettingSchema.plugin(autoIncrement.plugin, {
// 	model: "Setting",
// 	startAt: 1
// });

SettingSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

SettingSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

let Setting = mongoose.model("Setting", SettingSchema);

module.exports = Setting;
