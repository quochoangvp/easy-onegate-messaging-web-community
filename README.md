B1.	Cài đặt Nodejs (Dùng ổn định với v10.19.0 trên Ubuntu và v11.14.0 trên Windows)

B2.	Cài đặt Mongodb (Dùng ổn định với v3.6.8 trên cả Ubuntu và Windows)

B3.	Cài đặt Redis (v6.0.6)

B4.	Đăng ký 01 ứng dụng Facebook và xin các quyền được mô tả tại key FACEBOOK_SCOPE trong file biến môi trường tương ứng (.env_prod hoặc .env_dev). Nhập vào dữ liệu FACEBOOK_CLIENTID, FACEBOOK_CLIENTSECRET, FACEBOOK_CALLBACKURL, FACEBOOK_VTOKEN được cung cấp vào file môi trường.

B5.	Đăng ký tài khoản Google Firebase và điền thông tin vào file biến môi trường tương ứng (.env_prod hoặc .env_dev)

B6.	Cài đặt các module npm bằng lệnh: npm install

B7.	Với môi trường development, chạy lệnh npm run dev. App sẽ chạy trên cổng 3000. Truy cập https://localhost:3000

	Với môi trường production thì:

	+ Build client trước bằng lệnh: npm run build

	+ Sau đó build server bằng lệnh: npm run build:server

	+ Cuối cùng chạy mod production bằng lệnh: npm run start:bundle

