"use strict";


module.exports = {

	// Secret for ID hashing
	hashSecret: "TdfKoVWjirFyWZWriHdb6cthQah2kSvDM40wax97g1Q",
	// Secret for session hashing
	sessionSecret: "av41cOqNpOmMFpBivF1HFCho3XhRAU3UvtWxQv0vPJH",
	// Application settings

	app: {
		title: process.env.APP_TITLE,
		version: process.env.APP_VERSION,
		description: process.env.APP_DESCRIPTION,
		keywords: process.env.APP_KEYWORDS,
		url: process.env.APP_URL,
		contactEmail: process.env.APP_CONTACTEMAIL,
	},

	// Database (Mongo) settings
	db: {

		uri:   process.env.MONGO_URI,
		adminUri: process.env.MONGO_URI,
		host: process.env.HOST,
		client_db_sufix: 'bt',
		client_db_template: process.env.MANTIS_TEMPLATE_DB,
		options: {
			db: { native_parser: true },
			server: { poolSize: 5 },
			user: process.env.MONGO_USERNAME,
			pass: process.env.MONGO_PASSWORD,
			auth: { authdb: "admin" }
		}
	},

	// Redis settings for caching
	redis: {
		enabled: false,
		uri: process.env.REDIS_URI,
		options: null
	},

	// elastic search settings for full text search
	elastic: {
		enabled: process.env.ELASTIC_ENABLED == 'true',
		host1 : {
			host: process.env.ELASTIC_URI,
			port: process.env.ELASTIC_PORT,
			protocol: process.env.ELASTIC_PROTOCOL,
			auth: process.env.ELASTIC_AUTH_USERNAMEL + ":" + process.env.ELASTIC_AUTH_PASS
		}
	},

	// Mail sending settings
	mailer: {
		enabled: process.env.MAILER_ENABLED,
		from: process.env.MAILER_FROM,
		transport: process.env.MAILER_TRANSPORT,
		sendgrid: {
			apiKey: process.env.MAILER_SENDGRID_APIKEY
		}
	},

	// Features of application
	features: {
		disableSignUp: process.env.FEATURES_DISABLESIGNUP==='true',
		verificationRequired: process.env.FEATURES_VERIFICATIONREQUIRED==='true',
		enableNormalLogin: process.env.FEATURES_ENABLENORMALLOGIN==='true',
	},

	// Social authentication (OAuth) keys
	authKeys: {

		google: {
			// clientID: "644973297979-fkp7tg38rs62hf5p0naiqmgcve48ltg1.apps.googleusercontent.com",
			// clientSecret: "fsVsVQfgmc1KTcrrm5QzQ0YX"
		},

		facebook: {
			// bos.edu.vn Test1
			clientID: process.env.FACEBOOK_CLIENTID,
			clientSecret: process.env.FACEBOOK_CLIENTSECRET,
			callbackURL: process.env.FACEBOOK_CALLBACKURL,
			scope: process.env.FACEBOOK_SCOPE.split(','),
			webhookVerifyToken: process.env.FACEBOOK_VTOKEN
		},
		github: {
			// clientID: "ea8fbbc8e05173c175df",
			// clientSecret: "3b65096a707fbc9d0481831f501142604320ce2e"
		},

		twitter: {
			clientID: null,
			clientSecret: null
		}
	},

	// Logging settings
	logging: {

		console: {
			// level: "debug"
		},

		file: {
			enabled: false,
			// path: path.join(global.rootPath, "logs"),
			// level: "info",
			// json: false,
			// exceptionsSeparateFile: true
		},

		graylog: {
			enabled: false
			// servers: [ { host: "192.168.0.100", port: 12201 } ]
		},

		papertrail: {
			enabled: false,
			host: null,
			port: null,
			level: "debug",
			program: "mantis"
		},

		logentries: {
			enabled: false,
			token: null
		},

		loggly: {
			enabled: false,
			token: null,
			subdomain: null
		},

		logsene: {
			enabled: false,
			token: null
		},

		logzio: {
			enabled: false,
			token: null
		}

	}

};

