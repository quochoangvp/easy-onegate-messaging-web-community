export function overview(state) {
	return state.overview;
}

export function recentlyMessageAndContactCount(state) {
	return state.recentlyMessageAndContactCount;
}

export function messageAndCommentTodayCount(state) {
	return state.messageAndCommentTodayCount;
}

export function topUserInteraction(state) {
	return state.topUserInteraction;
}

export function customerInteractionRecently(state) {
	return state.customerInteractionRecently;
}

export function newStatistics(state) {
	return state.newStatistics;
}

export function pageStatistics(state) {
	return state.pageStatistics;
}

export function staffs(state) {
	return state.staffs;
}

export function staffStatistics(state) {
	return state.staffStatistics;
}

export function interactionStatistics(state) {
	return state.interactionStatistics;
}

export function totalInteractionStatistics(state) {
	return state.totalInteractionStatistics;
}

export function tags(state) {
	return state.tags;
}

export function tagStatistics(state) {
	return state.tagStatistics;
}

export function todayTagStatistics(state) {
	return state.todayTagStatistics;
}
