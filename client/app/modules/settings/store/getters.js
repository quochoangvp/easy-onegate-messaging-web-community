export function settings(state) {
	return state.rows;
}

export function selected(state) {
	return state.selected;
}
export function getByType(state, type) {
	return state.rows.map(row=>{
		return row.type === type;
	});
	//return state.rows
}
export function fetching(state) {
	return state.fetching;
}
export function sort(state) {
	return state.sort;
}

export function viewMode(state) {
	return state.viewMode;
}
