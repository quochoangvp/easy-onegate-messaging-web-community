export const SEARCH						= "SEARCH";
export const ADD_NOTIFICATION 			= "ADD_NOTIFICATION";
export const ADD_MESSAGE 				= "ADD_MESSAGE";
export const SET_USER	 				= "SET_SESSION_USER";
export const SET_SESSION_CRM	 		= "SET_SESSION_CRM";
export const CLEAR_SESSION_CRM	 		= "CLEAR_SESSION_CRM";
export const CLEAR_NOTIFICATION			= "CLEAR_ALL_NOTIFICATION";
export const CLEAR_MESSAGE              = "CLEAR_MESSAGE";