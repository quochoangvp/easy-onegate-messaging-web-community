module.exports = {
	settingTypes: [
	 
		{
			id: "messages",
			name: "Messages",
			detail: "Detail",
			template: []
		},
		{
			id: "pages",
			name: "Pages",
			detail: "Detail",
			template: []
		},
		{
			id: "conversations",
			name: "Conversations",
			detail: "Detail",
			template: []
		},
		{
			id: "system",
			name: "System",
			detail: "Detail",
			template: []
		},
		{
			id: "job",
			name: "Job",
			detail: "Detail",
			template: []
		}
	],
	inputTypes: [
		{ id: "number", name: "Number", detail: "Detail" },
		{ id: "checkbox", name: "Checkbox", detail: "Detail" },
		{ id: "radio", name: "Radio", detail: "Detail" },
		{ id: "select", name: "select", detail: "Detail" },
		{ id: "date", name: "Date", detail: "Detail" },
		{ id: "dateTime", name: "DateTime", detail: "Detail" }
	],
	templates: [
		{
			id: "conversations",
			name: "Conversations",
			pid: "notifications",
			detail: "Setting notification check conversations"
		},
		{
			id: "messaging",
			name: "Messages",
			pid: "notifications",
			detail: "Setting notification check messages"
		},
		{
			id: "system",
			name: "System",
			pid: "notifications",
			detail: "Setting notification check system"
		},
		{
			id: "activity",
			pid: "notifications",
			name: "Activity",
			detail: "Setting notification check activity"
		},
		{
			id: "online",			 
			name: "OnlineUserStatus",
			pid: "system",
			detail: "Setting system check online user"
		}
	]
};
