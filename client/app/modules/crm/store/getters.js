export function contact(state) {
	return state.contact;
}

export function orders(state) {
	return state.orders;
}

export function products(state) {
	return state.products;
}

export function sales(state) {
	return state.sales;
}
