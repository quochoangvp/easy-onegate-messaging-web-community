export function messages(state) {
	return state.rows;
}
export function paging(state){
	return state.paging;
}
export function ptoken(state) {
	return state.ptoken;
}
export function conversations(state) {
	return state.conversations;
}
export function posts(state) {
	return state.posts;
}

export function hasMore(state) {
	return state.hasMore;
}

export function fetching(state) {
	return state.fetching;
}

export function sort(state) {
	return state.sort;
}

export function viewMode(state) {
	return state.viewMode;
}
