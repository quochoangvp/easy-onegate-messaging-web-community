import Vue from "vue";
import moment from "moment";
import { ROLE } from "./types";
import { validators } from "vue-form-generator";


import { find } from "lodash";

let _ = Vue.prototype._;

module.exports = {

	id: "users",
	title: _("Users"),

	table: {
		multiSelect: true,
		columns: [
			{
				title: _("ID"),
				field: "code",
				align: "left",
				formatter(value, model) {
					return model ? model.code : "";
				}
			},
			{
				title: _("Provider"),
				field: "provider",
				align: "left",
				//  formatter(value, model) {
				// 	return model ? model.profile.provide : "";
				//  }
			},

			{
				title: _("Roles"),
				field: "roles",
				formatter(value) {
					//let type = find(ROLE, (role) => role.id == value);
					return value.join(";"); //role ? role.name : value;
				}
			},
			{
				title: _("Pages"),
				field: "profile.accessPages",
				formatter(value) {
					//let type = find(ROLE, (role) => role.id == value);
					return value ? "<i class='fa fa-page'/>" : "<i class='fa fa-ban'/>";
					//return value.join(";"); //role ? role.name : value;
				}
			},
			{
				title: _("Address"),
				field: "address"
			},
			{
				title: _("Email"),
				field: "email",
				align: "left",
			},
			{
				title: _("Name"),
				field: "fullName",
				align: "left",
			},
			{
				title: _("Status"),
				field: "status",
				formatter(value, model, col) {
					return value ? "<i class='fa fa-check'/>" : "<i class='fa fa-ban'/>";
				},
				align: "center"
			},
			{
				title: _("LastCommunication"),
				field: "lastCommunication",
				formatter(value) {
					return moment(value).fromNow();
				}
			}
		],

		rowClasses: function(model) {
			return {
				inactive: !model.status
			};
		}

	},

	form: {
		fields: [
			{
				type: "text",
				label: _("ID"),
				model: "code",
				readonly: true,
				disabled: true,
				multi: false,
				get(model) {
					if (model.code)
						return model.code;
					else
						return _("willBeGenerated");
				}
			},
			{
				type: "text",
				label: _("Provider"),
				model: "provider",
				readonly: true,
				disabled: true,
				multi: false,
				// get(model) {
				// 	if (model.code)
				// 		return model.code;
				// 	else
				// 		return _("willBeGenerated");
				// }
			},

			{
				type: "select",
				label: _("Roles"),
				model: "roles",
				required: true,
				values: ROLE,
				default: "user",
				multi: true,
				validator: validators.required

			},	
			{
				type: "select",
				label: _("Pages"),
				model: "profile.accessPages",
				required: true,
				//values: ROLE,
				//default: "user",
				multi: true,
				validator: validators.required

			},	
			{
				type: "text",
				label: _("Email"),
				model: "email",
				required: true,
				//values: this.,
				//default: "new",
				validator: validators.required

			},	
			{
				type: "text",
				label: _("Name"),
				model: "fullName",
				featured: true,
				required: true,
				placeholder: _("ContactName"),
				validator: validators.string
			},
			{
				type: "text",
				label: _("Description"),
				model: "description",
				featured: false,
				required: false,
				validator: validators.string
			},	
			{
				type: "text",
				label: _("Address"),
				model: "address",
				placeholder: _("AddressOfContact"),
				validator: validators.string
			},
			{
				type: "label",
				label: _("LastCommunication"),
				model: "lastCommunication",
				get(model) { return model && model.lastCommunication ? moment(model.lastCommunication).fromNow() : "-"; }
			},
			{
				type: "switch",
				label: _("Status"),
				model: "status",
				multi: true,
				default: 1,
				textOn: _("Active"),
				textOff: _("Inactive"),
				valueOn: 1,
				valueOff: 0
			}
		]
	},

	options: {
		searchable: true,


		enableNewButton: true,
		enabledSaveButton: true,
		enableDeleteButton: true,
		enableCloneButton: false,

		validateAfterLoad: false, // Validate after load a model
		validateAfterChanged: false, // Validate after every changes on the model
		validateBeforeSave: true // Validate before save a model
	},

	events: {
		onSelect: null,
		onNewItem: null,
		onCloneItem: null,
		onSaveItem: null,
		onDeleteItem: null,
		onChangeItem: null,
		onValidated(model, errors, schema) {
			if (errors.length > 0)
				console.warn("Validation error in page! Errors:", errors, ", Model:", model);
		}
	},

	resources: {
		addCaption: _("AddNewContact"),
		saveCaption: _("Save"),
		cloneCaption: _("Clone"),
		deleteCaption: _("Delete")
	}

};