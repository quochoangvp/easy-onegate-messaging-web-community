import Vue from "vue";
import toastr from "../../../core/toastr";
//import window.conversationService from "../../../core/window.conversationService";
import {
	LOAD,
	LOAD_MORE,
	ADD,
	UPDATE,
	VOTE,
	UNVOTE,
	REMOVE,
	NO_MORE_ITEMS,
	FETCHING,
	CHANGE_SORT,
	CHANGE_VIEWMODE,
	CLEAR,
	ROW_CHANGED,
} from "./types";

export const NAMESPACE = "/api/conversations";

//let window.conversationService = window.conversationwindow.conversationService;

export const clear = function({ commit, state }) {
	commit(CLEAR);
};

export const fb = function({ commit, state }, fbModel) {
	return window.conversationService.rest("fb", fbModel);
};
export const fbConversations = function({ commit, state }, params) {
	commit(FETCHING, true);
	let fbModel = params.fbModel;
	let loadMore = params.loadMore;
	if (loadMore && state.paging.cursors.after && fbModel[2])
		fbModel[2].after = state.paging.cursors.after;
	return window.conversationService
		.rest("fbConversations", fbModel)
		.then(data => {
			console.log("conversations", data);
			if (!data.paging || !data.paging.next) commit(NO_MORE_ITEMS);
			//else
			commit(loadMore ? LOAD_MORE : LOAD, data);
		})
		.catch(err => {
			toastr.error(err);
		})
		.then(() => {
			commit(FETCHING, false);
		});

	//return window.conversationService.rest("fbConversations" , fbmodel);
};
export const getRows = function({ commit, state }, params) {
	commit(FETCHING, true);
	// console.log("conversations window.getRows", params);
	let loadMore = params.loadMore;
	if(!loadMore) commit(CLEAR);
	// let condition = params.condition;
	// let sort = params.sort?params.sort: state.sort;
	// return window.conversationService
	// 	.rest("find", {
	// 		filter: state.viewMode,
	// 		condition,
	// 		sort,
	// 		limit: 20,
	// 		offset: state.offset
	// 	})
	// 	.then(data => {
	// 		if (data.length == 0) commit(NO_MORE_ITEMS);
	// 		else commit(loadMore ? LOAD_MORE : LOAD, data);
	// 	})
	// 	.catch(err => {
	// 		toastr.error(err.message);
	// 	})
	// 	.then(() => {
	// 		commit(FETCHING, false);
	// 	});

	return window.conversationService.rest("find",Object.assign({offset: state.offset},params))
		.then(data => {
			if (data.length == 0) commit(NO_MORE_ITEMS);
			else commit(loadMore ? LOAD_MORE : LOAD, data);
		})
		.catch(err => {
			toastr.error(err.message);
		})
		.then(() => {
			commit(FETCHING, false);
		});

};
export const getRow = function({ commit, state }, params) {
	console.log("conversations window.getRow", params);
	return window.conversationService
		.rest("get", params);
};

export const findRowBy = ({ commit }, row) => {
	return window.conversationService.rest("findBy", row);
};

export const search = ({commit, state}, params) => {
	commit(FETCHING, true);
	console.log("conversations window.search", params);
	let loadMore = params.loadMore;
	if(!loadMore) commit(CLEAR);
	let condition = params.condition;
	let filter = params.filter;
	return window.conversationService
		.rest("search", {
			filter,
			condition,
			sort: state.sort,
			limit: 100,
			offset: state.offset
		})
		.then(data => {
			console.log("result", data);
			if (data.length == 0) commit(NO_MORE_ITEMS);
			else commit(loadMore ? LOAD_MORE : LOAD, data);
		})
		.catch(err => {
			toastr.error(err);
		})
		.then(() => {
			commit(FETCHING, false);
		});
	//return window.conversationService.rest("search", params);
};

export const updateRowBy = ({ commit }, row) => {
	return window.conversationService.rest("updateBy", row);
};

export const loadMoreRows = function(context, params) {
	params.loadMore = true;
	return getRows(context, params);
};

export const changeSort = function(store, sort) {
	store.commit(CHANGE_SORT, sort);
	getRows(store);
};

export const changeViewMode = function(store, viewMode) {
	store.commit(CHANGE_VIEWMODE, viewMode);
	getRows(store);
};

export const saveRow = function(store, model) {
	return window.conversationService.rest("updateOrInsert", model);
};

export const updateRow = function(store, model) {
	window.conversationService
		.rest("update", model)
		.then(data => {
			updated(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const removeRow = function(store, model) {
	window.conversationService
		.rest("remove", { code: model.code })
		.then(data => {
			removed(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const vote = function(store, model) {
	window.conversationService
		.rest("vote", { code: model.code })
		.then(data => {
			updated(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const unVote = function(store, model) {
	window.conversationService
		.rest("unvote", { code: model.code })
		.then(data => {
			updated(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const created = function({ commit }, model) {
	commit(ADD, model);
};

export const updated = function({ commit }, model) {
	commit(UPDATE, model);
};

export const removed = function({ commit }, model) {
	commit(REMOVE, model);
};

export const updateRowStore = function({ commit, state }, item) {
	commit(ROW_CHANGED, item);

};
